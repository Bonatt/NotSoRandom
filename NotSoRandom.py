import ROOT
import math
import random
import numpy as np

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
def sin(x):
  return math.sin(x)
# Redefine pi to something shorter
pi = math.pi
print ''




#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(56); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y. 56:lo=y/hi=r. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();





### Random number generator, except each time it "goes left or right", make it also more likely to go left, right respectively.
# Then add some function that fights it towards center?









cr = ROOT.TCanvas('cr', 'cr', 1366, 720) #1366,720,455
'''
c.Divide(1,3)
c.cd(1)
#c.SetTicks()
ROOT.gPad.SetBottomMargin(0)
#ROOT.gPad.SetRightMargin(1.1)
c.cd(2)
ROOT.gPad.SetTopMargin(0)
#ROOT.gPad.SetRightMargin(1.1)
ROOT.gPad.SetBottomMargin(0)
c.cd(3)
ROOT.gPad.SetTopMargin(0)
#ROOT.gPad.SetRightMargin(1.1)
'''
cdf = ROOT.TCanvas('cdf', 'cdf', 1366, 720)
cdf.Divide(1,2)
cdf.cd(1)
ROOT.gPad.SetBottomMargin(0)
cdf.cd(2)
ROOT.gPad.SetTopMargin(0)


G = []
Gf = []
Gd = []

Loops1 = 20 # Only two ~unique colors for kRed-10 to kRed+10, e.g.
Loops2 = 5000
for l in xrange(Loops1):

  i = [0] 	# Number of cycles. Initialize first index.
  r = [0]	# How far from zero
  dr = 0.1	# Step size from previous r, 0.01
  d = 0.	# Initial change in d
  dd = 0#.001	# Step size from previous d, 0.001
  dList = [0]

  p = 0.5	# Percent split

  maxr = 100   # 20
  #f = sin(d)	# Initial restoring force 
  k = 0#.0001 	# 0.001
  f = -k*r[0]
  fList = [0]

  for ll in xrange(Loops2):
    if (random.random()+d+f) < p:
      r.append(round(r[ll]-dr,2))
      d -= dd
      #f += sin(maxr)/sin(d)
      f -= k*r[ll]
      ### Combinations of f here and below (+=/+=, +=/-=, -=/+=, -=/-=) make different shaped r
      #  +/-: +"s" to max, -linear to -max.
      #  +/+: +linear to max, -linear to -max.
      #  -/+: +linear to max, -"s" to -max.
      #  -/-: sin, triangle wave functions
    else:
      r.append(round(r[ll]+dr,2))
      d += dd
      #f += -sin(maxr)/sin(d)
      f -= k*r[ll] #+=

    dList.append(d)
    fList.append(f)
    i.append(float(ll))
    if abs(r[ll]) >= maxr: break

  g = ROOT.TGraph(len(i), np.array(i), np.array(r))
  g.SetLineColor(ROOT.kViolet-9+l)#l+1)
  G.append(g)

  gd = ROOT.TGraph(len(i), np.array(i), np.array(dList))
  gd.SetLineColor(ROOT.kTeal-9+l)#l+1)
  Gd.append(gd)

  gf = ROOT.TGraph(len(i), np.array(i), np.array(fList))
  gf.SetLineColor(ROOT.kOrange-9+l)#l+1)
  Gf.append(gf)

  if l == 0:
    cr.cd()
    G[l].SetTitle(';Iteration;R_{i} = R_{i-1} + D_{i-1} + F_{i-1}')
    G[l].GetXaxis().SetLimits(0, Loops2)#ll*1.1)
    G[l].GetYaxis().SetRangeUser(-maxr*1.1, maxr*1.1)
    #G[l].GetYaxis().SetRangeUser(-max(r)*1.1, max(r)*1.1)
    G[l].Draw('al')
  
    cdf.cd(1)
    Gd[l].SetTitle(';Iteration;D_{i} = D_{i-1} #pm '+str(dd))
    Gd[l].GetXaxis().SetLimits(0, Loops2)#ll*1.1)
    Gd[l].GetYaxis().SetRangeUser(-maxr*0.01, maxr*0.01)
    #Gd[l].GetYaxis().SetRangeUser(-max(dList)*1.1, max(dList)*1.1)
    Gd[l].Draw('al')
  
    cdf.cd(2)
    Gf[l].SetTitle(';Iteration;F_{i} = -'+str(k)+' R_{i-1}')
    Gf[l].GetXaxis().SetLimits(0, Loops2)#ll*1.1)
    Gf[l].GetYaxis().SetRangeUser(-maxr*0.05, maxr*0.05)
    #Gf[l].GetYaxis().SetRangeUser(-max(fList)*1.1, max(fList)*1.1)
    Gf[l].Draw('al')


  else:
    cr.cd()
    G[l].Draw('l same')
  
    cdf.cd(1)
    Gd[l].Draw('l same')
  
    cdf.cd(2)
    Gf[l].Draw('l same')


'''
cr.cd()
legend = ROOT.TLegend(0.15, 0.65, 0.33, 0.85)
legend.AddEntry(g,  'p = rand(0,1) + d_{i-1} + f_{i-1}','')
legend.AddEntry(g,  '    if p < 0.5: s = -1','')
legend.AddEntry(g,  '    if p > 0.5: s = +1','')
#legend.AddEntry(g,  'r_{i} = r_{i-1} + '+str(dr)+' s','l') #r_{i-1}) + d_{i-1} + f_{i-1}','l')
##legend.AddEntry(g,  'r_{i} = r_{i-1} + d_{i-1} + f_{i-1}','l')
#legend.AddEntry(gd, 'd_{i} = d_{i-1} + '+str(dd)+' s', 'l')
#legend.AddEntry(gf, 'f_{i} = -'+str(k)+' r_{i-1}', 'l')
legend.SetFillColor(0);
legend.Draw()
'''

cr.SaveAs('NotSoRandom_dR='+str(dr)+'_dD='+str(dd)+'_K='+str(k)+'_R.png')
#cr.SaveAs('NotSoRandom_dr='+str(dr)+'_dd='+str(dd)+'_k='+str(k)+'_r.pdf')

cdf.SaveAs('NotSoRandom_dR='+str(dr)+'_dD='+str(dd)+'_K='+str(k)+'_DF.png')
#cdf.SaveAs('NotSoRandom_dr='+str(dr)+'_dd='+str(dd)+'_k='+str(k)+'_df.pdf')

### Point of no return:
# p * dr/dd
#print 'Point of no return?:', p*dr/dd
