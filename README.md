### Creating a random walk with hysteresis

I thought about how a 1D random walk would behave if its current choice was based on its previous choice. 
<!--I modelled this as some displacement from zero, and if some threshold was met, the walks current trend would continue. -->

Consider the parameters necessary to create a random walk with hysteresis, that is, if the walk previously moved "left/right",
the probability of it currently moving "left/right" is higher (excuse the lack of LaTeX support here):
```python
R = 0.	      # Current distance from origin, 0 initially
dR = 0.1      # Step size from previous R

D = 0.        # Initial change in D
dD = 0.001    # Step size from previous D

P = 0.5	      # Percent split. 2D random walk means 50% chance to move left, 50% right
```

And now consider the logic below with these parameters in use:
```python
N = random.random()

if (N + D) < P:
    R -= dR
    D -= dD
else:
    R += dR
    D += dD
```

Hopefully one can see that if (N + D) is below P, R will decrease by its stepsize dR, 
and D will decrease by its stepsize dD. On the following iteration, (N + D) will be more likely to be below P. 
And so on. These parameters are logged and plotted for 20 walks:
![R: dR = 0.1, dD = 0.001](NotSoRandom_dR=0.1_dD=0.001_K=0_R.png)
![D, F: dR = 0.1, dD = 0.001](NotSoRandom_dR=0.1_dD=0.001_K=0_DF.png)

You can see that there exists a point of no return around R = 50. Specfically, when D = P, R will continue 
at a constant rate from 0 indefinitely. 
Generally, this is equal to P * dR/dD.

I then wondered what would happen if a restoring force F was _also_ introduced (seen above as 0):
```python
K = 0.0001
F = -K * R

N = random.random()

if (N + D + F) < P:
    F -= K*R
    R -= dR
    D -= dD
else:
    F -= K*R
    R += dR
    D += dD
```

Again, these parameters are logged and plotted below:
![R: dR = 0.1, dD = 0.001, K = 0.0001](NotSoRandom_dR=0.1_dD=0.001_K=0.0001_R.png)
![D, F: dR = 0.1, dD = 0.001, K = 0.0001](NotSoRandom_dR=0.1_dD=0.001_K=0.0001_DF.png)

Now, instead of continuing at a constant rate indefinitely, the restoring force attempts to return R to 0. 
Because of the values chosen for K and dD, these paremters will oscillate indefinitely but with an amplitude that increases with each iteration:
![R: dR = 0.1, dD = 0.001, K = 0.0001, High iterations](NotSoRandom_dR=0.1_dD=0.001_K=0.0001_R_long.png)
![D, F: dR = 0.1, dD = 0.001, K = 0.0001, High iterations](NotSoRandom_dR=0.1_dD=0.001_K=0.0001_DF_long.png)


And of course if dD and K are set to zero we get a typical 1D random walk:
![R: dR = 0.1, dD = 0, K = 0](NotSoRandom_dR=0.1_dD=0_K=0_R.png)
<!--![D, F: dR = 0.1, dD = 0, K = 0](NotSoRandom_dR=0.1_dD=0_K=0_DF.png)-->

There (always) is more fun mathematical analyses to be had here but this was where I shelved this project.
